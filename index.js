var findRoot = require('find-root'),
  resolve = require('resolve'),
  path = require('path'),
  fs = require('fs'),
  isAbsolute = path.isAbsolute || require('is-absolute'),
  SystemJS = require('systemjs');

const log = require('debug')('eslint-plugin-import:resolver:systemjs');


let systemJsInitialized = false;

exports.interfaceVersion = 2;
exports.resolve = function (source, file, configPath) {
  let packageDir;
  if (!systemJsInitialized &&  (!configPath || !isAbsolute(configPath)))  {
    packageDir = findRoot(path.resolve(file))
    if (!packageDir){
      throw new Error('package not found above ' + file);
    } 
    var maybePath = path.resolve(path.join(packageDir, configPath));
    try {
      const st = fs.statSync(maybePath);
      systemJsConfig = require(maybePath);
      SystemJS.config(systemJsConfig);
      systemJsInitialized = true;
    } catch (e) {
        log('no system js config');
    }

  }
  const name = SystemJS.normalizeSync(source, file).replace('file:', '');
  try {
    stats = fs.statSync(name);
    return { found: true, path: name }
  } catch (e) {
    return { found: false, }
  }
}

